package fourth.task2;

import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Customer customer1 = new Customer("Иванов", "Иван", "Иванович", "Москва", "5123", "6543");
        Customer customer2 = new Customer("Молотков", "Петр", "Евгеньевич", "Санкт-Петербург", "5432", "1234");
        Customer customer3 = new Customer("Степанов", "Олег", "Евгеньевич", "Екатеринбург", "33432", "13234");
        Customer customer4 = new Customer("Жукова", "Ирина", "Васильевна", "Новосибирск", "3432", "21234");
        Customer customer5 = new Customer("Глебова", "Ольга", "Анатольевна", "Хабаровск", "1432", "12134");

        List<Customer> customerList = new LinkedList<>();
        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);
        customerList.add(customer4);
        customerList.add(customer5);

        int max = 0;
        int maxIndex = -1;
        for (int i = 0; i < customerList.size(); i++) {
            if (customerList.get(i).getSurnameLength() > max) {
                max = customerList.get(i).getSurnameLength();
                maxIndex = i;
            }
        }

        System.out.println("Имя покупателя с самой длинной фамилией: " + customerList.get(maxIndex).getName());

        String result_address = "";
        for (int i = 0; i < customerList.size(); i++) {
            if (customerList.get(i).getCreditCardNumber().charAt(0) == '5') {
                result_address += "\n" + customerList.get(i).getAddress();
            }
        }

        if (result_address.isEmpty()) {
            System.out.println("Среди покупателей нет людей, у которых первая цифра номера кредитки равна 5");
        } else {
            System.out.println("Адреса всех покупателей, у кого первая цифра номера кредитки 5: " + result_address);
        }

        String result_FIO = "";
        for (int i = 0; i < customerList.size(); i++) {
            if (customerList.get(i).getPatronymic() == "Евгеньевич") {
                result_FIO += "\n" + customerList.get(i).getFio();
            }
        }

        if (result_FIO.isEmpty()) {
            System.out.println("Среди покупателей нет людей с отчеством Евгеньевич");
        } else {
            System.out.println("Всех покупателей с отчеством Евгеньевич: " + result_FIO);
        }
    }
}