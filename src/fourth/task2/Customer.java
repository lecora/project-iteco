package fourth.task2;

public class Customer {
    private String surname;
    private String name;
    private String patronymic;
    private String address;
    private String creditCardNumber;
    private String bankAccountNumber;

    public Customer(String surname, String name, String patronymic, String address, String creditCardNumber, String bankAccountNumber) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.address = address;
        this.creditCardNumber = creditCardNumber;
        this.bankAccountNumber = bankAccountNumber;
    }

    public int getSurnameLength() {
        return surname.length();
    }

    public String getFio() {
        String Fio = surname + " " + name + " " + patronymic;
        return Fio;
    }

    public String getName() {
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public String getAddress() {
        return address;
    }
}