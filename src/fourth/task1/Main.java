package fourth.task1;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person();
        Person person2 = new Person("Sysueva Valeriia Vladimirovna", (byte) 22);

        person1.printPersonInfo();
        person2.printPersonInfo();
    }
}