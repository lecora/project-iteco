package fourth.task1;

import java.util.Random;

public class Person {
    private static final byte DEFAULT_AGE = 30;
    private String fullName;
    private Byte age;

    public void move() {
        System.out.println(this.fullName + " говорит");
    }

    public void talk() {
        System.out.println(this.fullName + " ходит");
    }

    public Person() {
        this("person" + new Random().nextInt(10), DEFAULT_AGE);
    }

    public Person(String fullName, Byte age) {
        this.fullName = fullName;
        this.age = age;
    }

    public void printPersonInfo() {
        System.out.println("Full name " + fullName + ", age " + age);
    }
}