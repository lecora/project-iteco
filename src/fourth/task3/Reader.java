package fourth.task3;

public class Reader {
    private String FIO;
    private Integer readerTicket;
    private String faculty;
    private String birthDate;
    private String phoneNumber;
    private Byte numberOfBooks;
    private String nameOfBooks;

    public Reader(String FIO, Integer readerTicket, String faculty, String birthDate, String phoneNumber) {
        this.FIO = FIO;
        this.readerTicket = readerTicket;
        this.faculty = faculty;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
    }

    public void takeBook(Byte numberOfBooks) {
        System.out.println(this.FIO + " взял " + numberOfBooks + " книги");
    }

    public void takeBook(String nameOfBooks) {
        System.out.println(this.FIO + " взял книги: " + nameOfBooks);
    }

    public void takeBook(Book nameBook) {
        System.out.println(this.FIO + " взял книги: " + nameBook.getNameBook());
    }

    public void returnBook(Byte numberOfBooks) {
        System.out.println(this.FIO + " вернул " + numberOfBooks + " книги");
    }

    public void returnBook(String nameOfBooks) {
        System.out.println(this.FIO + " вернул книги: " + nameOfBooks);
    }

    public void returnBook(Book nameBook) {
        System.out.println(this.FIO + " вернул книги: " + nameBook.getNameBook());
    }
}