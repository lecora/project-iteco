package fourth.task3;

public class Main {
    public static void main(String[] args) {
        Book book1 = new Book("Петров", "Приключения");
        Book book2 = new Book("Иванов", "Словарь");
        Book book3 = new Book("Сидоров", "Энциклопедия");

        Reader reader1 = new Reader("Павлов", 123, "БИТ", "01.01.1991", "79000123456");
        Reader reader2 = new Reader("Молотов", 3123, "ФБИТ", "02.02.1992", "79654321000");

        reader1.takeBook((byte) 3);
        reader1.takeBook("Приключения, Словарь, Энциклопедия");
        reader1.returnBook((byte) 3);
        reader2.takeBook(book3);
        reader2.returnBook(book2);
        reader2.returnBook("Словарь");
    }
}