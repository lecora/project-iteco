package fourth.task3;

public class Book {
    private String author;
    private String nameBook;

    public Book(String author, String nameBook) {
        this.author = author;
        this.nameBook = nameBook;
    }

    public String getNameBook() {
        return nameBook;
    }
}