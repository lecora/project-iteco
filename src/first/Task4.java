package first;

public class Task4 {
    public static void main(String[] args) {

        System.out.println("int: от "+Integer.MIN_VALUE+" до "+Integer.MAX_VALUE);
        System.out.println("long: от "+Long.MIN_VALUE+" до "+Long.MAX_VALUE);
        System.out.println("short: от "+Short.MIN_VALUE+" до "+Short.MAX_VALUE);
        System.out.println("byte: от "+Byte.MIN_VALUE+" до "+Byte.MAX_VALUE);
    }
}
