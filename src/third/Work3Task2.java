package third;

import java.util.ArrayList;
import java.util.PriorityQueue;

public class Work3Task2 {
    public static void main(String[] args) {
        bank();
    }

    public static void bank() {
        int K = 2; // количество окон
        int N = 4; // количество человек
        int time = 10; // время на обслуживание
        int M = 15; // количество минут от начала смены

        PriorityQueue<String> visitors = new PriorityQueue<>();
        visitors.add("Игорь");
        visitors.add("Катя");
        visitors.add("Лена");
        visitors.add("Миша");
//        visitors.add("Никита");
//        visitors.add("Петя");

        String visitorResult = "";

        int formula = M / time * K; // сколько людей обслуживается за 10 минут
        for (int i = 0; i < formula; i++) {
            if (visitors.size() == 0) {
                break;
            } else {
                visitors.remove();
            }
        }

        ArrayList<String> visitorsArray = new ArrayList<>();
        if (formula != 0) {
            for (int i = 0; i < formula; i++) {
                if (!visitors.isEmpty()) {
                    visitorsArray.add(visitors.peek());
                    visitors.remove();
                } else if (visitors.isEmpty() && visitorsArray.isEmpty()) {
                    System.out.println("В очереди никого нет на данный момент");
                    break;
                }
            }
        } else {
            for (int i = 0; i < K; i++) {
                visitorsArray.add(visitors.peek());
                visitors.remove();
            }
        }

        for (int i = 0; i < visitorsArray.size(); i++) {
            visitorResult += visitorsArray.get(i);
            if (visitorsArray.size() > 1 && i < formula - 1) {
                visitorResult += " и ";
            } else if (visitorsArray.size() > 1 && i < K - 1) {
                visitorResult += " и ";
            }
        }

        if (visitorResult.isEmpty()) {
            System.out.println("на " + M + " минуте у окон никого не будет");
        } else {
            System.out.println("на " + M + " минуте у окон будет/будут стоять " + visitorResult);
        }
    }
}