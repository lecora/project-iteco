package third;

import java.util.*;

public class Work3Task3 {
    public static void main(String[] args) {
        duplicateElements();
    }

    public static void duplicateElements() {
        ArrayList<Integer> array = new ArrayList<>();
        Random random = new Random();
        HashMap<Integer, Integer> duplicate = new HashMap<>();
        int count = 1;

        for (int i = 0; i < 10; i++) {
            array.add(random.nextInt(7));
        }

        for (int i = 0; i < array.size(); i++) {
            if (!duplicate.containsKey(array.get(i))) {
                duplicate.put(array.get(i), count);
            } else {
                duplicate.computeIfPresent(array.get(i), (k, v) -> v + 1);
            }
        }

        HashMap<Integer, Integer> result = new HashMap<>();
        for (Map.Entry<Integer, Integer> entry : duplicate.entrySet()) {
            if (entry.getValue() != 1) {
                result.put(entry.getKey(), entry.getValue());
            }
        }

        System.out.println(array);
        System.out.println(result);
    }
}