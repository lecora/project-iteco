package third;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Work3Task1 {
    public static void main(String[] args) {
        medianOfTwoArrays();
    }

    public static void medianOfTwoArrays() {
        List<Integer> list1 = Arrays.asList(1, 3);
        List<Integer> list2 = Arrays.asList(2);
        List<Integer> result = new ArrayList<>();

        int pointer1 = 0;
        int pointer2 = 0;
        while (pointer1 < list1.size() && pointer2 < list2.size()) {
            if (list1.get(pointer1) < list2.get(pointer2)) {
                result.add(list1.get(pointer1));
                pointer1++;
            } else {
                result.add(list2.get(pointer2));
                pointer2++;
            }
        }
        if (pointer1 < list1.size()) {
            while (pointer1 < list1.size()) {
                result.add(list1.get(pointer1));
                pointer1++;
            }
        }
        if (pointer2 < list2.size()) {
            while (pointer2 < list2.size()) {
                result.add(list1.get(pointer2));
                pointer2++;
            }
        }

        int medianaIndex = 0;
        float mediana = 0;
        if (result.size() % 2 != 0) {
            medianaIndex = (result.size() / 2);
            mediana = result.get(medianaIndex);
        } else {
            medianaIndex = (result.size() / 2);
            mediana = (float) (result.get(medianaIndex) + result.get(medianaIndex - 1)) / 2;
        }

        System.out.println(mediana);
    }
}