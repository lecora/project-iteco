package second;

public class Work2Task5 {
    public static void main(String[] args) {
        String str = " 1 груша 2 яблока 3 апельсинаа 4 банана";
        int count_1 = 0;
        int count_2 = 0;
        int count_3 = 0;

        if (!str.isEmpty()) {
            for (int i = 0; i < str.length(); i++) {
                switch (str.charAt(i)) {
                    case '1':
                        count_1 += 1;
                        break;
                    case '2':
                        count_2 += 1;
                        break;
                    case '3':
                        count_3 += 1;
                        break;
                }
            }
            System.out.println("Количество 1: " + count_1 + "\nКоличество 2: "
                    + count_2 + "\nКоличество 3: " + count_3);
        } else {
            System.out.println("Пустая строка");
        }
    }
}