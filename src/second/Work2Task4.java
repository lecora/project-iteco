package second;

public class Work2Task4 {
    public static void main(String[] args) {
        // б) сумма цифр N
        int n1 = 123456;
        int n1_1 = n1;
        int sumN1 = 0;

        while (n1_1 != 0) {
            sumN1 += (n1_1 % 10);
            n1_1 /= 10;
        }
        System.out.println("Сумма цифр в числе " + n1 + " равна " + sumN1);

        // в) произведение цифр N
        int n2 = 123456;
        int n2_2 = n2;
        int prodn2 = 1;

        if (n2_2 == 0) {
            System.out.println("Введеное число равно нулю");
        } else {
            while (n2_2 != 0) {
                prodn2 *= (n2_2 % 10);
                n2_2 /= 10;
            }
            System.out.println("Произведение цифр числа " + n2 + " равно " + prodn2);
        }
    }
}