package second;

public class Work2Task1 {
    public static void main(String[] args) {
        int count = 2;
        double countPow;
        String parityCheck;

        if (count % 2 == 0) {
            parityCheck = " четное, ";
            countPow = Math.pow(count, 10);
        } else {
            parityCheck = " нечетное, ";
            countPow = Math.pow(count, 3);
        }

        System.out.println("Число " + count + parityCheck
                + "ответ: " + countPow);
    }
}
