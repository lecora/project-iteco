package second;

public class Work2Task3 {
    public static void main(String[] args) {
        String str = " привет,   в этой    строке лишние   пробелы.  ";
        StringBuilder builder = new StringBuilder();

        if (str.length() > 1) {
            for (int i = 0; i < str.length() - 1; i++) {
                if (str.charAt(i) != ' ') {
                    builder.append(str.charAt(i));
                } else if (str.charAt(i) == ' ' && str.charAt(i + 1) == ' ') {
                    continue;
                } else if (!builder.isEmpty()) {
                    builder.append(str.charAt(i));
                }
            }
        } else {
            System.out.println("В строке меньше двух символов");
        }

        String result = builder.toString();

        System.out.println(result);
    }
}
